﻿using Accelist.TrainingCenter.WPFExam.Models;
using Accelist.TrainingCenter.WPFExam.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Accelist.TrainingCenter.WPFExam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.BookService = new BookService();
        }

        private readonly BookService BookService;
        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var books = await BookService.DownloadAsync(null);
            if (books != null)
            {
                this.BookDataGrid.ItemsSource = books;
                this.AuthorCountDataGrid.ItemsSource = BookService.UpdateAuthorCounter(books);
                this.YearlyBookDataGrid.ItemsSource = BookService.UpdateYearlyBook(books);
                this.DetailedYearlyBookDataGrid.ItemsSource = BookService.UpdateDetailedYearlyBook(books);
            }
        }

        private async void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            var books = await BookService.DownloadAsync(this.SearchTextBox.Text);
            this.BookDataGrid.ItemsSource = null;
            this.BookDataGrid.ItemsSource = books;
        }

        private async void CreateANewBookButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.TitleTextBox.Text == "")
            {
                MessageBox.Show("Book's title cannot be empty");
                return;
            }
            if (this.TitleTextBox.Text.Length > 256)
            {
                MessageBox.Show("Book's title cannot be more than 256 characters");
                return;
            }
            if (this.AuthorTextBox.Text == "")
            {
                MessageBox.Show("Author's name cannot be empty");
                return;
            }
            if (this.AuthorTextBox.Text.Length > 256)
            {
                MessageBox.Show("Author's name cannot be more than 256 characters");
                return;
            }
            var book = new Book
            {
                Title = this.TitleTextBox.Text,
                Author = this.AuthorTextBox.Text
            };
            if (await BookService.CreateNewAsync(book) == true)
            {
                MessageBox.Show("Your new book has been successfully added!");
                this.TitleTextBox.Text = "";
                this.AuthorTextBox.Text = "";

                this.BookDataGrid.ItemsSource = null;
                var books = await BookService.DownloadAsync(null);
                this.BookDataGrid.ItemsSource = books;

                this.AuthorCountDataGrid.ItemsSource = null;
                this.AuthorCountDataGrid.ItemsSource = BookService.UpdateAuthorCounter(books);
            }
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            this.BookDataGrid.ItemsSource = null;
            var books = await BookService.DownloadAsync(null);
            this.BookDataGrid.ItemsSource = books;

            this.AuthorCountDataGrid.ItemsSource = null;
            this.AuthorCountDataGrid.ItemsSource = BookService.UpdateAuthorCounter(books);
        }
    }
}
