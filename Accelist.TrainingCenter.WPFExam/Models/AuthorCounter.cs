﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.WPFExam.Models
{
    public class AuthorCounter
    {
        public string AuthorName { get; set; }
        public int BookCount { get; set; }

        public DateTime LastReleasedBook { get; set; }
    }
}
