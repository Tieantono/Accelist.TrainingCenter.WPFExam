﻿using Accelist.TrainingCenter.WPFExam.Models;
using Accelist.TrainingCenter.WPFExam.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Accelist.TrainingCenter.WPFExam.Services
{
    public class BookService
    {
        public async Task<List<Book>> DownloadAsync(string search)
        {
            var client = new HttpClient();
            var statusCode = "";
            try
            {
                var content = await client.GetAsync($"http://dev.accelist.com/Library/api/v1/book?q={search}");
                content.EnsureSuccessStatusCode();
                statusCode = content.StatusCode.ToString();
                var books = JsonConvert.DeserializeObject<List<Book>>(await content.Content
                    .ReadAsStringAsync());
                return books.Select(Q => new Book
                {
                    BookId = Q.BookId,
                    Title = Q.Title,
                    Author = Q.Author,
                    CreationDate = Q.CreationDate.ToLocalTime()
                }).ToList();
            }
            catch (HttpRequestException e)
            {
                MessageBox.Show($@"{ e.Message }
{ statusCode }");
                
                return null;
            }
        }

        public async Task<bool> CreateNewAsync(Book book)
        {
            var client = new HttpClient();
            var jsonString = JsonConvert.SerializeObject(book);
            var statusCode = "";
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            try
            {
                var response = await client.PostAsync("http://dev.accelist.com/Library/api/v1/book", content);
                response.EnsureSuccessStatusCode();
                return true;
            }
            catch (HttpRequestException e)
            {
                MessageBox.Show($@"{ e.Message }
{ statusCode }");
                return false;
            }
        }

        public void RefreshBooks()
        {

        }

        public List<AuthorCounter> UpdateAuthorCounter(List<Book> books)
        {
            var authorList = books.GroupBy(Q => Q.Author, StringComparer.InvariantCultureIgnoreCase)
                .Select(Q => new AuthorCounter {
                    AuthorName = Q.Key,
                    BookCount = Q.Count(),
                    LastReleasedBook = Q.Max(X => X.CreationDate)
                })
                .OrderBy(Q => Q.AuthorName)
                .ToList();
            return authorList;
        }

        public List<YearlyBook> UpdateYearlyBook(List<Book> books)
        {
            var yearList = books.GroupBy(Q => Q.CreationDate.Year.ToString())
                .Select(Q => new YearlyBook
                 {
                     CreationYear = Q.Key,
                     AuthorCount = Q.Select(X => X.Author).Distinct(StringComparer.InvariantCultureIgnoreCase).Count(),
                     TitleCount = Q.Count(),
                 })
                .OrderByDescending(Q => Q.CreationYear)
                .ToList();
            return yearList;
        }

        public List<DetailedYearlyBook> UpdateDetailedYearlyBook(List<Book> books)
        {
            var yearList = books.GroupBy(Q => Q.CreationDate.Year.ToString())
                .Select(Q => new DetailedYearlyBook
                {
                    CreationYear = Q.Key,
                    Author = string.Join("\n", Q.Select(X => X.Author)),
                    Title = string.Join("\n", Q.Select(X => X.Title)),
                    CreationDate = Q.Select(X => X.CreationDate.ToString("dd/MMM/yyyy")).Aggregate((a, b) => a + "\n" + b)
                })
                .OrderByDescending(Q => Q.CreationYear)
                .ToList();
            return yearList;
        }
    }
}
