﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.WPFExam.ViewModels
{
    public class YearlyBook
    {
        public string CreationYear { get; set; }
        public int AuthorCount { get; set; }
        public int TitleCount { get; set; }
    }
}
